function realizarOperaciones() {
    var numero1 = parseFloat(document.getElementById("numero1").value);
    var numero2 = parseFloat(document.getElementById("numero2").value);

    var resultados = document.getElementById("resultados");
    resultados.innerHTML = "";

    resultados.innerHTML += "<strong> Los resultados de las operaciones son los siguientes: </strong><br>"; // Agregando la frase
    resultados.innerHTML += "<br>"; // Agregando la frase
   
    for (var i = 1; i <= 5; i++) {
        var resultado;

        switch (i) {
            case 1:
                resultado = numero1 + numero2;
                resultados.innerHTML += "Suma: " + resultado + "<br>";
                break;
            case 2:
                resultado = numero1 - numero2;
                resultados.innerHTML += "Resta: " + resultado + "<br>";
                break;
            case 3:
                resultado = numero1 * numero2;
                resultados.innerHTML += "Multiplicación: " + resultado + "<br>";
                break;
            case 4:
                resultado = numero1 / numero2;
                resultados.innerHTML += "División: " + resultado + "<br>";
                break;
            case 5:
                resultado = numero1 % numero2;
                resultados.innerHTML += "MOD: " + resultado + "<br>";
                break;
        }
    }
}
